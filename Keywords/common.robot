*** Variables ***
${browser}    chrome
${chromedriver_path}    C:/Python3/chromedriver_win32/chromedriver.exe

*** Keywords ***

Open browser with ${url} in ${browser} 
    Open Browser  ${url}  ${browser}   executable_path=${chromedriver_path}
    Maximize Browser Window
    Login to admin
    
Login to admin
     Sleep  10s
   Input Text Xpath Element Visible    //input[@id='email']        ${username}
   Input Text Xpath Element Visible    //input[@type='password']    ${password}
   Click Xpath Element Visible        //button[@type='submit']
   
Input Text Xpath Element Visible
    [Arguments]    ${xpath}    ${value}
    Wait Until Element Is Visible    ${xpath}  timeout=20s  error=Element ${xpath} not found
    Input Text    ${xpath}    ${value}
    sleep    1s
    
Click Xpath Element Visible
    [Arguments]    ${xpath}
    Wait Until Element Is Visible    ${xpath}  timeout=20s  error=Element ${xpath} not found
    Click Element    ${xpath}
    sleep    2s
    

